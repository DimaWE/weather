<?php

use yii\db\Migration;

/**
 * Handles the creation of table `weather`.
 */
class m190128_194504_create_weather_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE IF NOT EXISTS `weather` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `date` date NOT NULL,
                `morning` json NOT NULL,
                `day` json NOT NULL,
                `evening` json NOT NULL,
                `night` json NOT NULL,
                PRIMARY KEY  (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;   
            
            ALTER TABLE `weather`
            ADD UNIQUE KEY `date` (`date`);
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('weather');
    }
}
