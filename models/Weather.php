<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "weather".
 *
 * @property int $id
 * @property string $date
 * @property array $morning
 * @property array $day
 * @property array $evening
 * @property array $night
 */
class Weather extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'weather';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'morning', 'day', 'evening', 'night'], 'required'],
            [['date', 'morning', 'day', 'evening', 'night'], 'safe'],
            [['date'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'morning' => 'Morning',
            'day' => 'Day',
            'evening' => 'Evening',
            'night' => 'Night',
        ];
    }

    public function getM(): array {
        return json_decode($this->morning, true);
    }

    public function getD(): array {
        return json_decode($this->day, true);
    }

    public function getE(): array {
        return json_decode($this->evening, true);
    }

    public function getN(): array {
        return json_decode($this->night, true);
    }
}
