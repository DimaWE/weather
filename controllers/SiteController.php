<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Weather;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(): string
    {
        $weatherModel = Weather::findOne(['date' => date("Y-m-d")]);
        return $this->render('index', ['weatherModel' => $weatherModel]);
        
    }

    public function actionWeek(): string {
        $date = date("Y-m-d");
        $dateWeek = date("Y-m-d",strtotime($date."+6 DAY"));
        $weatherModels = Weather::find()
            ->where(['>=', 'date', $date])
            ->andWhere(['<=', 'date', $dateWeek])
            ->all();
        return $this->render('week', [
            'weatherModels' => $weatherModels,
            'date' => $date,
            'dateWeek' => $dateWeek,
        ]);
    }
}
