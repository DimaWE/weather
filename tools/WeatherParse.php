<?php
namespace app\tools;

use phpQuery;
use Curl\Curl;

class WeatherParse
{
    private $url;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    public function getWeatherHTML(): string 
    {
        $curl = new Curl();

        //только для локального сервера, на реальных проектах никогда не использовать
        $curl->setOpt(CURLOPT_SSL_VERIFYPEER, false);

        $curl->get($this->url);

        if(!$curl->response)
            throw new \Error($curl->error_message, $curl->error_code);
        else       
            return $curl->response;
    }

	public function weatherParse(): array {

		$html = $this->getWeatherHTML();
		$pq = phpQuery::newDocumentHTML($html);
		$weather = [];
		$date = date("Y-m-d");

		foreach ($pq->find("tbody.weather-table__body") as $day) {
		  $vs = $day->firstChild;
		  $weatherDay = [];

		  while ($vs != false) {
		    $el = pq($vs);
		    $daypart = $el->find("div.weather-table__daypart")->elements[0]->textContent;
            $t = $el->find("div.weather-table__temp span.temp__value")->elements;
            $condition = $el->find("td.weather-table__body-cell_type_condition")->elements[0]->textContent;


            $daypartText = '';
            switch ($daypart) {
                case 'утром':
                    $daypartText = "morning";
                    break;
                case 'днём':
                    $daypartText = "day";
                    break;
                case 'вечером':
                    $daypartText = "evening";
                    break;
                case 'ночью':
                    $daypartText = "night";
                    break;
            }


		    $weatherDay[$daypartText] = [
                'tMin' => $t[0]->textContent ?? null, 
                'tMax' => $t[1]->textContent ?? $t[0]->textContent?? null, 
                'condition' => $condition ?? null,
            ];

		    $vs = $vs->nextSibling;
		  }

		  $weatherDay['date'] = $date;
		  $weather[] = $weatherDay;

		  $date = date("Y-m-d",strtotime($date."+1 DAY"));

		}
        
		return $weather;
	}
}
  