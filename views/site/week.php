<?php

/* @var $this yii\web\View */

$this->title = 'Погода на неделю';
?>


<div class="container">
    <?php if($weatherModels): ?>
        <div class="page-header">
            <h1>Погода на неделю <small><?=$date?> - <?=$dateWeek?></small></h1>
        </div>

        <?php foreach ($weatherModels as $weatherModel): ?>

            <div>
                <h2><?=$weatherModel->date?></h2>
            </div>

            <div class="weath">
                <div class="daypart">Утром</div>
                <div class="temp"><?=$weatherModel->m['tMin']?>...<?=$weatherModel->m['tMax']?> <small><?=$weatherModel->m['condition']?></small></div>
            </div>

            <div class="weath">
                <div class="daypart">Днем</div>
                <div class="temp"><?=$weatherModel->d['tMin']?>...<?=$weatherModel->d['tMax']?> <small><?=$weatherModel->d['condition']?></small></div>
            </div>

            <div class="weath">
                <div class="daypart">Вечером</div>
                <div class="temp"><?=$weatherModel->e['tMin']?>...<?=$weatherModel->e['tMax']?> <small><?=$weatherModel->e['condition']?></small></div>
    
            </div>

            <div class="weath">
                <div class="daypart">Ночью</div>
                <div class="temp"><?=$weatherModel->n['tMin']?>...<?=$weatherModel->n['tMax']?> <small><?=$weatherModel->n['condition']?></small></div>
            </div>

        <?php endforeach; ?>

    <?php else: ?>
        <div class="alert alert-warning" role="alert">Нет данных о погоде</div>
    <?php endif; ?>
</div>


